package models

import (
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func UpdateMassageTask(task MassageTask) (params dynamodb.UpdateItemInput) {
	sstar := strconv.FormatInt(task.Start, 10)
	send := strconv.FormatInt(task.End, 10)
	spredict := strconv.FormatInt(task.PredictNext, 10)
	ssess := strconv.Itoa(task.Session)
	stotal := strconv.Itoa(task.TotalSess)

	params = dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{ // Required
			"id": {
				S: aws.String(task.MassagerID),
			},
		},
		TableName:        aws.String("massagerstatus"), // Required
		UpdateExpression: aws.String("SET #name = :nameval, #start = :startval, #end = :endval, #session = :sessval, #totalsess = :totalval, #predict = :predictval"),
		ExpressionAttributeNames: map[string]*string{ // Required
			"#name":      aws.String("massager"),
			"#start":     aws.String("start"),
			"#end":       aws.String("end"),
			"#session":   aws.String("session"),
			"#totalsess": aws.String("totalsess"),
			"#predict":   aws.String("predict"),
		},
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":nameval":    {S: aws.String(task.Massager)},
			":startval":   {N: aws.String(sstar)},
			":endval":     {N: aws.String(send)},
			":sessval":    {N: aws.String(ssess)},
			":totalval":   {N: aws.String(stotal)},
			":predictval": {N: aws.String(spredict)},
		},
	}

	return params
}

/*DynamoGetMassager Get Massager table by id*/
func DynamoGetMassager(id string) (param dynamodb.GetItemInput) {
	params := dynamodb.GetItemInput{
		TableName: aws.String("massager"),
		Key: map[string]*dynamodb.AttributeValue{ // Required
			"id": { // Required
				S: aws.String(id),
			},
		},
	}
	return params
}

/*DynamoGetMassageTask Get Massage Task table by id*/
func DynamoGetMassageTask(id string) (param dynamodb.GetItemInput) {
	params := dynamodb.GetItemInput{
		TableName: aws.String("massagerstatus"),
		Key: map[string]*dynamodb.AttributeValue{ // Required
			"id": { // Required
				S: aws.String(id),
			},
		},
	}
	return params
}

/*DynamoGetBatch get all id and task param*/
func DynamoGetBatch(id []string) (param dynamodb.BatchGetItemInput) {
	ids := []string{"1", "2"}
	var mykeys []map[string]*dynamodb.AttributeValue
	for _, val := range ids {
		mp1 := map[string]*dynamodb.AttributeValue{
			"id": {S: aws.String(val)},
		}
		mykeys = append(mykeys, mp1)
	}

	params := dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{ // Required
			"massagerstatus": { // Required
				Keys: mykeys,
				//ProjectionExpression: aws.String("massagerid"),
				//AttributesToGet: []*string{aws.String("massagerid"), aws.String("start"), aws.String("end"), aws.String("session")},
			},
		},
		//ReturnConsumedCapacity: aws.String("TOTAL"),
	}
	return params
}

/*DynamoScanID scan trough massage table*/
func DynamoScanID() (params dynamodb.ScanInput) {
	params = dynamodb.ScanInput{
		TableName: aws.String("massager"), // Required
		AttributesToGet: []*string{
			aws.String("id"), // Required
		},
	}
	return params

}
