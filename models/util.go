package models

import (
	"log"
	"time"
)

/*Timezone map from  IANA Time Zone database*/
var Timezone = map[string]string{
	"Taiwan": "Asia/Taipei",
}

func tpTime() time.Time {
	twTime, err := timeIn("Taiwan")
	if err != nil {
		log.Println("convert to taiwan time error")
		return time.Now().UTC()
	}
	return twTime

}

func timeIn(name string) (time.Time, error) {
	loc, err := time.LoadLocation(Timezone[name])
	if err != nil {
		return time.Time{}, err
	}
	return time.Now().In(loc), nil
}

func IsSameDay(t1, t2 time.Time) bool {
	y1, m1, d1 := t1.Date()
	y2, m2, d2 := t2.Date()
	//	log.Println("t1:y:", y1, " m1:", m1, " d1:", d1)
	//	log.Println("t2:y:", y2, " m1:", m2, " d1:", d2)
	if y1 == y2 && m1 == m2 && d1 == d2 {
		return true
	}
	return false
}
