package models

import (
	"errors"
	"log"
	"massagedev/awshelper"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

/*Massager struct*/
type Massager struct {
	ID      string
	Name    string
	Pincode string
}

/*MassageTasks is a struct to record massage task*/
type MassageTask struct {
	MassagerID  string
	Massager    string
	Start       int64
	End         int64
	PredictNext int64
	Session     int
	TotalSess   int
}

/*NewTask newTask*/
func NewTask(id, name string, session, action int) bool {
	oldTask, err := GetTask(id)
	log.Println("oldTask:", oldTask)
	timeNow := tpTime()
	awsConf, _ := awshelper.InitConfig(awshelper.USEastVA)
	if err != nil { // dose not exit create one
		if action == 1 {
			newtask := MassageTask{MassagerID: id, Massager: name, Start: timeNow.Unix(), Session: session, TotalSess: 1}
			param := UpdateMassageTask(newtask)
			resp, err := awshelper.DynOp(param, awsConf)
			if err != nil {
				log.Println("NewTask create new err:", err)
				return false
			}
			log.Println("NewTask create new:", resp)
			return true
		}
		return false
	}

	if action != 1 { // end service just update the task
		oldTask.End = timeNow.Unix()
		param := UpdateMassageTask(oldTask)
		resp, err := awshelper.DynOp(param, awsConf)
		if err != nil {
			log.Println("update end time err:", err)
			return false
		}
		log.Println("End Service Time", resp)
		return true
	}

	oldstar := time.Unix(oldTask.Start, 0)
	if IsSameDay(oldstar, timeNow) {
		oldTask.Session = session
		oldTask.Start = timeNow.Unix()
		oldTask.End = 0
		oldTask.TotalSess = oldTask.TotalSess + session
		//one session is ten minute, 300 is rest time
		oldTask.PredictNext = oldTask.Start + int64(session)*(600) + 300
		//log.Println("is the same day")
	} else {
		oldTask.Session = session
		oldTask.Start = timeNow.Unix()
		oldTask.End = 0
		oldTask.TotalSess = session
		//one session is ten minute, 300 is rest time
		oldTask.PredictNext = oldTask.Start + int64(session)*(600) + 300
	}
	param := UpdateMassageTask(oldTask)
	resp, err := awshelper.DynOp(param, awsConf)
	if err != nil {
		log.Println("update end time err:", err)
		return false
	}
	log.Println("Service Start Time", resp)
	return true
}

/*IsMassager Check if massaer in database*/
func IsMassager(id string) bool {
	param := DynamoGetMassager(id)
	awsConf, _ := awshelper.InitConfig(awshelper.USEastVA)
	resp, err := awshelper.DynOp(param, awsConf)
	if err != nil {
		return false
	}
	output := (resp.(dynamodb.GetItemOutput))
	log.Println("IsMassager:", output)
	if len(output.Item) == 0 {
		return false
	}
	return true
}

/*GetMassager get Massage from db*/
func GetMassager(id string) (Massager, error) {
	param := DynamoGetMassager(id)
	awsConf, _ := awshelper.InitConfig(awshelper.USEastVA)
	resp, err := awshelper.DynOp(param, awsConf)
	if err != nil {
		return Massager{}, errors.New("dynamodb fail")
	}
	output := (resp.(dynamodb.GetItemOutput))
	if len(output.Item) == 0 {
		return Massager{}, errors.New("no id found in db")
	}
	pincodeval := aws.StringValue(output.Item["pincode"].S)
	nameval := aws.StringValue(output.Item["name"].S)
	log.Println("GetMassager:id", id, "pincode:", pincodeval)
	return Massager{ID: id, Pincode: pincodeval, Name: nameval}, nil
}

/*GetMassager get Massage from db*/
func GetTask(id string) (MassageTask, error) {
	param := DynamoGetMassageTask(id)
	awsConf, _ := awshelper.InitConfig(awshelper.USEastVA)
	resp, err := awshelper.DynOp(param, awsConf)
	if err != nil {
		return MassageTask{}, errors.New("dynamodb fail")
	}
	output := (resp.(dynamodb.GetItemOutput))
	if len(output.Item) == 0 {
		return MassageTask{}, errors.New("no id found in db")
	}
	//	log.Println("GetTask output:", output)
	thetask, _ := GetMassageTask(output)
	return thetask, nil

}

func GetMassageTask(output dynamodb.GetItemOutput) (MassageTask, error) {
	theID := aws.StringValue(output.Item["id"].S)
	theName := aws.StringValue(output.Item["massager"].S)
	start := aws.StringValue(output.Item["start"].N)
	istart, _ := strconv.ParseInt(start, 10, 64)
	end := aws.StringValue(output.Item["end"].N)
	iend, _ := strconv.ParseInt(end, 10, 64)
	next := aws.StringValue(output.Item["predict"].N)
	inext, _ := strconv.ParseInt(next, 10, 64)

	sess := aws.StringValue(output.Item["session"].N)
	isess, _ := strconv.Atoi(sess)
	total := aws.StringValue(output.Item["totalsess"].N)
	itotal, _ := strconv.Atoi(total)

	return MassageTask{MassagerID: theID, Massager: theName, Start: istart, End: iend, PredictNext: inext, Session: isess, TotalSess: itotal}, nil
}

/*GetCurrentTask add a new task*/
func GetCurrentTask() ([]MassageTask, error) {
	var tasks []MassageTask
	param := DynamoScanID()
	awsConf, _ := awshelper.InitConfig(awshelper.USEastVA)
	resp, err := awshelper.DynOp(param, awsConf)
	if err != nil {
		log.Println("GetCurrentTask:err:", err)
		return tasks, err
	}
	var ids []string
	for _, val := range resp.(dynamodb.ScanOutput).Items {
		ids = append(ids, aws.StringValue(val["id"].S))
	}
	log.Println("get massager ids:", ids)

	for _, id := range ids {
		p := DynamoGetMassageTask(id)
		res, err := awshelper.DynOp(p, awsConf)
		if err != nil {
			log.Println("GetCurrentTask err:", err)
			return tasks, err
		}
		output := res.(dynamodb.GetItemOutput)
		thetask, _ := GetMassageTask(output)
		tasks = append(tasks, thetask)
	}

	return tasks, nil

}
