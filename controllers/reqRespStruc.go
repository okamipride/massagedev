package controllers

const STATUS_CODE_200 = 200
const STATUS_CODE_401 = 401

/*ResponseDefault default response*/
type ResponseDefault struct {
	StatusCode   int    `json:"statusCode"`
	ErrorMessage string `json:"errorMessage"`
}

/*ReqVerifyLocation to recieve request from massager to login store*/
type ReqVerifyLocation struct {
	UserID  string `json:"userID"`
	StoreID string `json:"storeID"`
}

/*RespCheckinSucc to return data to user*/
type RespCheckinSucc struct {
	StatusCode   int    `json:"statusCode"`
	ErrorMessage int    `json:"errorMessage"`
	UserID       string `json:"massagerID"`
	UserName     string `json:"massageName"`
	StoreID      string `json:"storeID"`
	Action       bool   `json:"action"`
}

/*ReqMassagerCheckin to recieve data from  Massager checkin checkout*/
type ReqMassagerCheckin struct {
	UserID  string `json:"userID"`
	StoreID string `json:"storeID"`
	Action  bool   `json:"action"` //true:checkin, false:checkout
}

/*ReqMassagerTimer to recieve data from  Massager checkin checkout*/
type ReqMassagerTimer struct {
	UserID  string `json:"id"`
	Pincode string `json:"pincode"`
	Session int    `json:"session"`
	Action  int    `json:"action"` //1:start, 0:stop
}

/*RespCountDown to response Count down request to user*/
type RespCountDown struct {
	StatusCode   int    `json:"statusCode"`
	ErrorMessage string `json:"errorMessage"`
	Countdown    int    `json:"timeleft"`
}

/*RespTotalSession to response total session of a massager*/
type RespTotalSession struct {
	StatusCode   int    `json:"statusCode"`
	ErrorMessage string `json:"errorMessage"`
	Total        int    `json:"total"`
}

/*RespLastedTask show latest task info*/
/*客人 要按多久 開始時間 預期的結束時間 結束時間 小時數 預測可被預約的開始時間*/
type RespLastedTask struct {
	StatusCode   int            `json:"statusCode"`
	ErrorMessage string         `json:"errorMessage"`
	Tasks        []LatestedTask `json:"tasks"`
}

/*LatestedTask task for RespLatestTask*/
type LatestedTask struct {
	ID           string `json:"id"`
	Massager     string `json:"massager"`
	Starttime    int64  `json:"start"`
	Endtime      int64  `json:"end"`
	Sesssion     int    `json:"session"`
	TotalSession int    `json:"totalsess"`
	PredNext     int64  `json:"predictNext"`
}
