package controllers

import (
	"massagedev/models"
	"time"

	"github.com/astaxie/beego"
)

/*InfoController the controller used to provide information*/
type InfoController struct {
	beego.Controller
}

/*LatestTasks list massagers status in a store*/
func (c *InfoController) LatestTasks() {
	tasks, err := models.GetCurrentTask()
	//log.Println("LatestTasks tasks", tasks)
	if err != nil {
		var resp ResponseDefault
		resp.StatusCode = STATUS_CODE_401
		resp.ErrorMessage = "get massager task error"
		c.Data["json"] = &resp
		c.ServeJSON()
	} else {
		ltask := convertMassageTaskToResp(tasks)
		ltask.StatusCode = STATUS_CODE_200
		c.Data["json"] = &ltask
		c.ServeJSON()
	}

}

func convertMassageTaskToResp(tasks []models.MassageTask) RespLastedTask {
	resp := RespLastedTask{}
	for _, task := range tasks {
		resptask := LatestedTask{ID: task.MassagerID, Massager: task.Massager, Starttime: task.Start,
			Endtime: task.End, Sesssion: task.Session, TotalSession: task.TotalSess, PredNext: task.PredictNext}
		resp.Tasks = append(resp.Tasks, resptask)
	}

	return resp

}

func mockLatestData() RespLastedTask {
	resp := RespLastedTask{}
	nowtime := time.Now().Unix()

	task1 := LatestedTask{ID: "1", Massager: "Young", Starttime: (nowtime - 300), Endtime: (nowtime - 300 + 4*10*60), Sesssion: 4, TotalSession: 20, PredNext: (nowtime - 300 + 4*10*60 + 300)}
	task2 := LatestedTask{ID: "2", Massager: "Marry", Starttime: (nowtime - 500), Endtime: (nowtime - 500 + 4*10*60), Sesssion: 4, TotalSession: 12, PredNext: (nowtime - 500 + 4*10*60 + 300)}
	task3 := LatestedTask{ID: "4", Massager: "Jason", Starttime: (nowtime - 600), Endtime: (nowtime - 600 + 5*10*60), Sesssion: 5, TotalSession: 20, PredNext: (nowtime - 600 + 5*10*60 + 300)}

	resp.Tasks = append(resp.Tasks, task1)
	resp.Tasks = append(resp.Tasks, task2)
	resp.Tasks = append(resp.Tasks, task3)
	return resp
}
