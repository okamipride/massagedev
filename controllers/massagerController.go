package controllers

import (
	"encoding/json"
	"log"
	"massagedev/models"

	"github.com/astaxie/beego"
)

/*MassagerController the controller used to record massager information*/
type MassagerController struct {
	beego.Controller
}

func (c *MassagerController) UserAuth() {
	userid := c.GetString("userid")
	pincode := c.GetString("pincode")
	log.Println("userid:", userid, "pincode", pincode)
	msger, err := models.GetMassager(userid)
	if err != nil {
		c.ResponseError(STATUS_CODE_401, "auth fail")
	} else {
		if msger.Pincode != pincode {
			c.ResponseError(STATUS_CODE_401, "auth fail")
		} else {
			c.ResponseDefaultOK()
		}

	}
}

/*Timer Massager to start a massage service*/
func (c *MassagerController) Timer() {
	var timer ReqMassagerTimer
	json.Unmarshal(c.Ctx.Input.RequestBody, &timer)
	msger, err := models.GetMassager(timer.UserID)
	log.Println("TimerID:", timer.UserID, " pin:", timer.Pincode, "sess:", timer.Session, " action:", timer.Action)
	if err != nil {
		log.Println("Timer GetMassager Error")
		c.ResponseError(STATUS_CODE_401, "auth fail")
	} else {
		if msger.Pincode != timer.Pincode {
			log.Println("Timer GetMassager Auth Fail")
			c.ResponseError(STATUS_CODE_401, "auth fail")
		} else {
			succ := models.NewTask(msger.ID, msger.Name, timer.Session, timer.Action)
			if !succ {
				log.Println("Timer GetMassager Create task Error")
				c.ResponseError(STATUS_CODE_401, "create task error")
			} else {
				log.Println("Timer GetMassager Success!")
				c.ResponseDefaultOK()
			}

		}
	}
}

/*VerifyLocation Massager to checkin in a store*/
func (c *MassagerController) VerifyLocation() {
	var useridstore ReqVerifyLocation
	json.Unmarshal(c.Ctx.Input.RequestBody, &useridstore)
	log.Println("userid:", useridstore, "store id")
	var verify ResponseDefault
	verify.StatusCode = STATUS_CODE_200
	c.Data["json"] = &verify
	c.ServeJSON()
}

/*Checkin Massager to checkin in a store*/
func (c *MassagerController) Checkin() {
	var checkin ReqMassagerCheckin
	json.Unmarshal(c.Ctx.Input.RequestBody, &checkin)
	log.Println("Checkin:", checkin.UserID, checkin.StoreID, checkin.Action)
	var resp ResponseDefault
	resp.StatusCode = STATUS_CODE_200
	c.Data["json"] = &resp
	c.ServeJSON()
}

/*CountDown Massager to get service left time*/
func (c *MassagerController) CountDown() {
	userid := c.GetString("userid")
	log.Println("userid:", userid)
	var resp RespCountDown
	resp.Countdown = 5
	resp.StatusCode = STATUS_CODE_200
	c.Data["json"] = &resp
	c.ServeJSON()
}

/*CountDown Massager to get service left time*/
func (c *MassagerController) GetSession() {
	userid := c.GetString("userid")
	log.Println("userid:", userid)
	if !models.IsMassager(userid) {
		c.ResponseError(STATUS_CODE_401, "no user found")
	} else {
		task, err := models.GetTask(userid)
		if err != nil {
			c.ResponseError(STATUS_CODE_401, "get task error")
		} else {
			var resp RespTotalSession
			resp.Total = task.TotalSess
			resp.StatusCode = STATUS_CODE_200
			c.Data["json"] = &resp
			c.ServeJSON()
		}
	}

}

func (c *MassagerController) ResponseError(errcode int, errStr string) {
	var resp ResponseDefault
	resp.StatusCode = errcode
	resp.ErrorMessage = errStr
	c.Data["json"] = &resp
	c.ServeJSON()
}

func (c *MassagerController) ResponseDefaultOK() {
	var resp ResponseDefault
	resp.StatusCode = STATUS_CODE_200
	log.Println("ResponseDefaultOK:", resp)
	c.Data["json"] = &resp
	c.ServeJSON()
}
