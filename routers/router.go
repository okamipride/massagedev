package routers

import (
	"massagedev/controllers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	//beego.Router("/", &controllers.MainController{})
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:    []string{"Origin", "Authorization", "Access-Control-Allow-Origin"},
		ExposeHeaders:   []string{"Content-Length", "Access-Control-Allow-Origin"},
	}))
	//Alexa API
	//beego.Router("/massager/verifylocation", &controllers.MassagerController{})
	beego.Router("/massager/verifylocation", &controllers.MassagerController{}, "post:VerifyLocation")
	beego.Router("/massager/checkin", &controllers.MassagerController{}, "post:Checkin")
	beego.Router("/massager/timer", &controllers.MassagerController{}, "post:Timer")
	beego.Router("/massager/auth", &controllers.MassagerController{}, "get:UserAuth")
	beego.Router("/massager/countdown", &controllers.MassagerController{}, "get:CountDown")
	beego.Router("/massager/session", &controllers.MassagerController{}, "get:GetSession")

	//InformationAPI
	beego.Router("/info/latesttask", &controllers.InfoController{}, "get:LatestTasks")
}
