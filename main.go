package main

import (
	_ "massagedev/routers"

	"github.com/astaxie/beego"
)

func main() {

	beego.Run()
}
