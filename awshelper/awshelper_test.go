package awshelper

import (
	"bytes"
	"log"
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func TestPutObject(t *testing.T) {
	conf, err := InitConfig(AsiaSL)
	if err != nil {
		log.Println("TestPutObject config Err:", err)
		t.Error("Expect init Config, get error:", err)
		os.Exit(1)
	}

	params := s3.PutObjectInput{
		Bucket: aws.String("climaxtestsdk"),
		Key:    aws.String("test.go"),
		ACL:    aws.String("public-read"),
		Body:   bytes.NewReader([]byte("PAYLOAD")),
	}

	resp, err := S3Op(params, conf)
	if err != nil {
		log.Println("S3 Error:", err)
		t.Error("Expect put object successfully but error:", err)
		os.Exit(1)
	} else {
		log.Println("s3 put object response", resp)
	}

}
