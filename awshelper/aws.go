package awshelper

import (
	//"errors"
	"log"
	"reflect"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/s3"
)

/* const for regions*/
const (
	USEastVA       = "us-east-1"
	USWestOR       = "us-west-2"
	USWestCA       = "us-west-1"
	EUIR           = "eu-west-1"
	EUFK           = "eu-central-1"
	AsiaSG         = "ap-southeast-1"
	AsiaSD         = "ap-southeast-2"
	AsiaTK         = "ap-northeast-1"
	AsiaSL         = "ap-northeast-2"
	SouthAmericaSP = "sa-east-1"
)
const (
	defaultBucket = "mybucket"
	// FOR Framwork
	//credFilePath = "../../../.aws/credentials"
	credFilePath = "/cygdrive/c/Users/C1150/.aws"
)
const awsProfile = "okami_virginia"

/*InitConfig initialized aws credentials */
func InitConfig(region string) (*aws.Config, error) {
	creds, conf := GetCredentialChain()
	val, err := creds.Get()
	if err != nil {
		log.Println("InitConfig:", err)
		return nil, err
	}
	log.Println("Cred ProviderName:", val.ProviderName)

	conf.WithRegion(region).WithCredentials(creds)
	return conf, nil
}

/*GetCredentialShared get shared credential profile*/
func GetCredentialShared() *credentials.Credentials {
	return credentials.NewSharedCredentials(credFilePath, awsProfile)
}

/*GetCredentialChain setup ChainCredential*/
func GetCredentialChain() (*credentials.Credentials, *aws.Config) {
	config := aws.NewConfig()
	ec2m := ec2metadata.New(session.New(), config)
	var ProviderList = []credentials.Provider{
		&ec2rolecreds.EC2RoleProvider{
			Client: ec2m,
		},
		&credentials.EnvProvider{},
		&credentials.SharedCredentialsProvider{Profile: awsProfile},
	}
	creds := credentials.NewChainCredentials(ProviderList)
	return creds, config
	//return credentials.NewStaticCredentials(accessKey, secretKey, ``)
}

/*ClwOp operation for dynamodb*/
func ClwOp(input interface{}, conf *aws.Config) (output interface{}, err error) {
	itype := reflect.TypeOf(input)
	//log.Println("ClwOp Name:", itype.Name())
	svc := cloudwatch.New(session.New(), conf)
	switch itype.Name() {
	case "PutMetricDataInput":
		puti := input.(cloudwatch.PutMetricDataInput)
		resp, operr := svc.PutMetricData(&puti)
		output = *resp
		err = operr
	}
	if err != nil {
		log.Println("ClwOp Err:", err)
		return nil, err
	}
	return output, nil
}

/*DynOp operation for dynamodb*/
func DynOp(input interface{}, conf *aws.Config) (output interface{}, err error) {
	itype := reflect.TypeOf(input)
	//log.Println("DynOp Name:", itype.Name())

	svc := dynamodb.New(session.New(), conf)
	switch itype.Name() {
	case "QueryInput":
		qi := input.(dynamodb.QueryInput)
		resp, operr := svc.Query(&qi)
		output = *resp
		err = operr
	case "GetItemInput":
		geti := input.(dynamodb.GetItemInput)
		resp, operr := svc.GetItem(&geti)
		output = *resp
		err = operr
	case "PutItemInput":
		puti := input.(dynamodb.PutItemInput)
		resp, operr := svc.PutItem(&puti)
		output = *resp
		err = operr
	case "DeleteItemInput":
		deli := input.(dynamodb.DeleteItemInput)
		resp, operr := svc.DeleteItem(&deli)
		output = *resp
		err = operr
	case "UpdateItemInput":
		updi := input.(dynamodb.UpdateItemInput)
		resp, operr := svc.UpdateItem(&updi)
		output = *resp
		err = operr
	case "ScanInput":
		sci := input.(dynamodb.ScanInput)
		resp, operr := svc.Scan(&sci)
		output = *resp
		err = operr

	case "BatchGetItemInput":
		log.Println("DynIp BatchGetItemInput")
		bti := input.(dynamodb.BatchGetItemInput)
		resp, operr := svc.BatchGetItem(&bti)
		output = *resp
		err = operr
	}

	if err != nil {
		log.Println("DynOp Err:", err)
		return nil, err
	}
	return output, nil
}

/*S3Op operations for s3*/
func S3Op(input interface{}, conf *aws.Config) (output interface{}, err error) {
	itype := reflect.TypeOf(input)
	log.Println("Name:", itype.Name())
	svc := s3.New(session.New(), conf)
	switch itype.Name() {
	case "PutObjectInput":
		po := input.(s3.PutObjectInput)
		resp, operr := svc.PutObject(&po)
		output = *resp
		err = operr
	case "DeleteObjectInput":
		do := input.(s3.DeleteObjectInput)
		resp, operr := svc.DeleteObject(&do)
		output = *resp
		err = operr
	}
	if err != nil {
		log.Println("S3Op Err:", err)
		return nil, err
	}
	return output, nil
}
